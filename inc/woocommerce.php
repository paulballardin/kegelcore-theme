<?php
/**
 * Add WooCommerce support
 *
 *
 * @package understrap
 */

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

//remove tabs

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}

//Create an action to use the full description

add_action('woocommerce_after_single_product_summary', 'woocommerce_full_description');

function woocommerce_full_description() {
	
	
	$output= 	'<div >';
  	$output.= 			'<div class="well">';
    $output.= 				get_the_content();
    $output.=				'<a  class="btn btn-primary more-info" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><span class="show-more">More</span><span style="display:none;" class="show-less">Less <i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></span></a>';
  	$output.= 			'</div>';
	$output.= 		'</div>';

	echo $output;
	
}

// Change number of thumbnail coumns to 4
add_filter ( 'woocommerce_product_thumbnails_columns', 'xx_thumb_cols' );
 function xx_thumb_cols() {
     return 5; // .last class applied to every 4th thumbnail
 }