<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */
 $the_theme = wp_get_theme();
?>
</div><!-- #page -->
</div> <!-- end .wrap -->
<?php get_sidebar('footerfull'); ?>

<section class="wrapper footer" id="wrapper-footer">

    <div class="section-content-inner clearfix">

            <div class="col-md-12">

                <footer id="colophon" class="site-footer" role="contentinfo">

                    <div class="site-info">
                         <?php wp_nav_menu(
                                    array(
                                        'theme_location' => 'footer',
                                        'menu_class' => 'nav navbar-nav ',
                                        'container' => 'text-xs-center',
                                        'fallback_cb' => '',
                                        'menu_id' => 'footer-menu'
                                    )
                            ); ?>
                            <div class="text-xs-center">&copy; Sugar and Sas Pty Ltd <?php echo date("Y") ?></div>
                    </div><!-- .site-info -->

                </footer><!-- #colophon -->

            </div><!--col end -->

</section><!-- wrapper end -->



<?php wp_footer(); ?>

</body>

</html>
